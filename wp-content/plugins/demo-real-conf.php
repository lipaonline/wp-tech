<?php
/*
Plugin Name: Démo CMB2 WP Tech 2015 (real data)
Description: Démo CMB2 pour le WP Tech 2015
Version: 1.0
Author: Patrick FAUST
Author URI: http://www.lipaonline.com/
License: GPLv2+
*/


// liste des conférenciers

function real_conf( $args ) {
    $real_conf = array(

        'mt' => 'Mathieu Chartier',
        'mj' => 'Maxime Bernard Jacquet',
        'jp' => 'Julio Potier',
        'ta' => 'Tony Archambeau',
        'rc' => 'Rémi Corson',
        'pd' => 'Pierre Dargham',
        'jo' => 'Julien Oger',
        'jb' => 'Jonathan Buttigieg',
        'pf' => 'Patrick Faust',
        'jb' => 'Jenny Beaumont',
        'fe' => 'Fabien Elharrar',


    );

    return $real_conf;


}
add_filter( '_wptech_demo_conf_filter', 'real_conf' );


// Post type de la metabox

function real_cpt( $args ) {
    $real_cpt = array('event');

    return $real_cpt;


}
add_filter( '_wptech_demo_cpt_filter', 'real_cpt' );

