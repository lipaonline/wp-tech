<?php

/**
 * Class PW_CMB2_Field_Google_Maps
 */
class PW_CMB2_Field_Google_Maps {

	/**
	 * Current version number
	 */
	const VERSION = '2.1.1';

	/**
	 * Initialize the plugin by hooking into CMB2
	 */
	public function __construct() {
		add_filter( 'cmb2_render_pw_map', array( $this, 'render_pw_map' ), 10, 5 );
		add_filter( 'cmb2_sanitize_pw_map', array( $this, 'sanitize_pw_map' ), 10, 4 );
	}

	/**
	 * Render field
	 */
	public function render_pw_map( $field, $field_escaped_value, $field_object_id, $field_object_type, $field_type_object ) {
		$this->setup_admin_scripts();


        echo"<div class='cmb-type-pw-map'>";


		echo '<input type="text" class="large-text pw-map-search" id="' . $field->args( 'id' ) . '" />';

		echo '<div class="pw-map"></div>';



        echo '<table>';

		echo  $field_type_object->input( array(
			'type'       => apply_filters( 'prod_text_hidden',  'text'),
			'name'       => $field->args('_name') . '[latitude]',
			'value'      => isset( $field_escaped_value['latitude'] ) ? $field_escaped_value['latitude'] : '',
			'class'      => 'pw-map-linput pw-map-latitude',
			'desc'       => '',
		) );

		echo  $field_type_object->input( array(
			'type'       => apply_filters( 'prod_text_hidden',  'text'),
			'name'       => $field->args('_name') . '[longitude]',
			'value'      => isset( $field_escaped_value['longitude'] ) ? $field_escaped_value['longitude'] : '',
			'class'      => 'pw-map-linput pw-map-longitude',
			'desc'       => '',
		) );

        echo "<tr><td>adresse 1 :</td><td>" . $field_type_object->input( array(
			'type'       => 'text',
			'name'       => $field->args('_name') . '[adress1]',
			'value'      => isset( $field_escaped_value['adress1'] ) ? $field_escaped_value['adress1'] : '',
			'class'      => 'pw-map-linput pw-map-adress',
			'desc'       => '',
		) );



        echo "</tr><tr><td>adresse 2 :</td><td>" . $field_type_object->input( array(
			'type'       => 'text',
			'name'       => $field->args('_name') . '[adress2]',
			'value'      => isset( $field_escaped_value['adress2'] ) ? $field_escaped_value['adress2'] : '',
			'class'      => 'pw-map-linput pw-map-adress',
            'number'       => 50,
			'desc'       => '',
		) );
         echo "</tr><tr></td><td>Code postal :</td><td>" . $field_type_object->input( array(
			'type'       => 'text',
			'name'       => $field->args('_name') . '[cp]',
			'value'      => isset( $field_escaped_value['cp'] ) ? $field_escaped_value['cp'] : '',
			'class'      => 'pw-map-cp',
			'desc'       => '',
		) );
         echo "</tr><tr></td><td>Ville :</td><td>" . $field_type_object->input( array(
			'type'       => 'text',
			'name'       => $field->args('_name') . '[ville]',
			'value'      => isset( $field_escaped_value['ville'] ) ? $field_escaped_value['ville'] : '',
			'class'      => 'pw-map-linput pw-map-ville',
			'desc'       => '',
		) );




        echo '</td></tr></table>';

        echo  $field_type_object->input( array(
			'type'       => apply_filters( 'prod_text_hidden',  'text'),
			'name'       => $field->args('_name') . '[zoom]',
			'value'      => isset( $field_escaped_value['zoom'] ) ? $field_escaped_value['zoom'] : '',
			'class'      => 'pw-map-sinput pw-map-zoom',
			'desc'       => '',
		) );
        echo "<br>";

        echo  $field_type_object->input( array(
			'type'       => apply_filters( 'prod_text_hidden',  'text'),
			'name'       => $field->args('_name') . '[datas]',
			'value'      => isset( $field_escaped_value['datas'] ) ? $field_escaped_value['datas'] : '',
			'class'      => 'pw-map-sinput pw-map-datas',
			'desc'       => '',
		) );



        echo '</div>';

	}

	/**
	 * Sanitize values
	 */
	public function sanitize_pw_map( $override_value, $value, $object_id, $field_args ) {




		return $value;
	}

	/**
	 * Enqueue scripts and styles
	 */
	public function setup_admin_scripts() {
		wp_register_script( 'pw-google-maps-api', '//maps.googleapis.com/maps/api/js?sensor=false&libraries=places', null, null );
		wp_enqueue_script( 'pw-google-maps', plugins_url( '../js/script.js', __FILE__ ), array( 'pw-google-maps-api' ), self::VERSION );
		wp_enqueue_style( 'pw-google-maps', plugins_url( '../css/style.css', __FILE__ ), array(), self::VERSION );
	}
}
$pw_cmb2_field_google_maps = new PW_CMB2_Field_Google_Maps();
