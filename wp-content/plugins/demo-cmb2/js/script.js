(function( $ ) {
	'use strict';

	var maps = [];

	$( '.cmb-type-pw-map' ).each( function() {
		initializeMap( $( this ) );
	});

	function initializeMap( mapInstance ) {
		var searchInput = mapInstance.find( '.pw-map-search' );
		var mapCanvas = mapInstance.find( '.pw-map' );
		var latitude = mapInstance.find( '.pw-map-latitude' );
        console.log( 'lat:' + latitude.val());
		var longitude = mapInstance.find( '.pw-map-longitude' );
        console.log(longitude);
        var adress = mapInstance.find( '.pw-map-adress' );
        var datas =  mapInstance.find( '.pw-map-datas' );
        var myzoom = mapInstance.find( '.pw-map-zoom' );
		var latLng = new google.maps.LatLng( 54.800685, -4.130859 );
		var zoom = 11;

		// If we have saved values, let's set the position and zoom level
		if ( latitude.val().length > 0 && longitude.val().length > 0 ) {
			latLng = new google.maps.LatLng( latitude.val(), longitude.val() );
			zoom = parseInt(myzoom.val()) || 11;

            console.log('zoom:' + zoom);
		}

		// Map
		var mapOptions = {
			center: latLng,
			zoom: zoom
		};
		var map = new google.maps.Map( mapCanvas[0], mapOptions );

		// Marker
		var markerOptions = {
			map: map,
			draggable: true,
			title: 'Drag to set the exact location'
		};
		var marker = new google.maps.Marker( markerOptions );

		if ( latitude.val().length > 0 && longitude.val().length > 0 ) {
			marker.setPosition( latLng );
		}

		// Search
		var autocomplete = new google.maps.places.Autocomplete( searchInput[0] );
		autocomplete.bindTo( 'bounds', map );

		google.maps.event.addListener( autocomplete, 'place_changed', function() {
			var place = autocomplete.getPlace();
			if ( ! place.geometry ) {
				return;
			}

			if ( place.geometry.viewport ) {
				map.fitBounds( place.geometry.viewport );
			} else {
				map.setCenter( place.geometry.location );
				map.setZoom( 17 );
			}

			marker.setPosition( place.geometry.location );

			latitude.val( place.geometry.location.lat() );
			longitude.val( place.geometry.location.lng() );
            adress.val(place.formatted_address);

            console.log(JSON.stringify(place.address_components)); // log address_components
            datas.val(JSON.stringify(place.address_components));


            // construct JSON datas for adress components
            var dat = '[' ;
            for(var i = 0; i < place.address_components.length; i += 1) {
                var addressObj = place.address_components[i];
                for(var j = 0; j < addressObj.types.length; j += 1) {

                   // console.log(addressObj.types[j]); // log address_components
                    dat += '{ \"' + addressObj.types[j] + '\" => ' ;
                  //  console.log(addressObj.long_name); // log address_components
                    dat += '\"' + addressObj.long_name + '\"' ;
                    dat += '},' ;



                    }
                }
                dat += ']';
                datas.val(dat);

		});

        // add listener to zoom button , and change field
        google.maps.event.addDomListener(map,'zoom_changed', function() {
            myzoom.val(map.getZoom());
        });


		$( searchInput ).keypress( function( event ) {
			if ( 13 === event.keyCode ) {
				event.preventDefault();
			}
		});

		// Allow marker to be repositioned
		google.maps.event.addListener( marker, 'drag', function() {
			latitude.val( marker.getPosition().lat() );
			longitude.val( marker.getPosition().lng() );
		});

		maps.push( map );
	}

	// Resize map when meta box is opened
	if ( typeof postboxes !== 'undefined' ) {
		postboxes.pbshow = function () {
			var arrayLength = maps.length;
			for (var i = 0; i < arrayLength; i++) {
				var mapCenter = maps[i].getCenter();
				google.maps.event.trigger(maps[i], 'resize');
				maps[i].setCenter(mapCenter);
			}
		};
	}

	// When a new row is added, reinitialize Google Maps
	$( '.cmb-repeatable-group' ).on( 'cmb2_add_row', function( event, newRow ) {
		var groupWrap = $( newRow ).closest( '.cmb-repeatable-group' );
		groupWrap.find( '.cmb-type-pw-map' ).each( function() {
			initializeMap( $( this ) );
		});
	});

})( jQuery );
