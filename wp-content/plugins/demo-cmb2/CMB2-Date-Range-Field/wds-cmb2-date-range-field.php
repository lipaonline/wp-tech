<?php



/**
 * Main initiation class
 */
class WDS_CMB2_Date_Range_Field {





	/**
	 * Sets up our plugin
	 * @since  0.1.0
	 */
	public function __construct() {
		add_filter( 'cmb2_render_date_range', array( $this, 'render' ), 10, 5 );
		add_filter( 'cmb2_sanitize_date_range', array( $this, 'sanitize' ), 10, 2 );
	}



	/**
	 * Renders the date range field in CMB2.
	 *
	 * @param object $field         The CMB2 Field Object.
	 * @param mixed  $escaped_value The value after being escaped, by default, with sanitize_text_field.
	 */
	public function render( $field, $escaped_value, $field_object_id, $field_object_type, $field_type ) {

		$this->setup_admin_scripts();


		// CMB2_Types::parse_args allows arbitrary attributes to be added
		$a = $field_type->parse_args( array(), 'input', array(
			'type'  => 'text',
			'class' => 'date-range button-secondary',
			'name'  => $field_type->_name(),
			'id'    => $field_type->_id(),
			'desc'  => $field_type->_desc( true ),
			'data-daterange' => json_encode( array(
				'id' => '#' . $field_type->_id(),
				'buttontext' => esc_attr( $field_type->_text( 'button_text', __( 'Dates...' ) ) ),
				'format' =>  'd-m-y',
			) ),
		) );

		printf( '<div class="cmb2-element"><input%s value=\'%s\'/></div>%s', $field_type->concat_attrs( $a, array( 'desc' ) ), json_encode( $escaped_value ), $a['desc'] );

	}

	/**
	 * Convert the json array made by jquery plugin to a regular array to save to db.
	 *
	 * @param mixed $override_value A null value as a placeholder to return the modified value.
	 * @param mixed $value The non-sanitized value.
	 *
	 * @return array|mixed An array of the dates.
	 */
	public function sanitize( $override_value, $value ) {

		$value = json_decode( $value, true );
		if ( is_array( $value ) ) {
			$value = array_map( 'sanitize_text_field', $value );
		} else {
			sanitize_text_field( $value );
		}

		return $value;

	}

    /**
	 * Enqueue scripts and styles
	 */
	public function setup_admin_scripts() {
    wp_enqueue_style( 'jquery-ui-daterangepicker', plugins_url(   '/assets/jquery-ui-daterangepicker/jquery.comiseo.daterangepicker.css', __FILE__ ), array(), '0.4.0' );
		wp_register_script( 'moment', plugins_url(  '/assets/moment.min.js', __FILE__ ), array(), '2.10.3' );
		wp_register_script( 'jquery-ui-daterangepicker', plugins_url(   '/assets/jquery-ui-daterangepicker/jquery.comiseo.daterangepicker.js', __FILE__ ), array( 'jquery', 'jquery-ui-core', 'jquery-ui-button', 'jquery-ui-menu', 'jquery-ui-datepicker', 'moment' ), '0.4.0' );
		wp_enqueue_script( 'cmb2-daterange-picker', plugins_url(   '/assets/cmb2-daterange-picker.js', __FILE__ ), array( 'jquery-ui-daterangepicker' ),  true );

    }


}

/**
 * Grab the WDS_CMB2_Date_Range_Field object and return it.
 * Wrapper for WDS_CMB2_Date_Range_Field::get_instance()
 */

$wds_cmb2_date_range_field = new WDS_CMB2_Date_Range_Field();


