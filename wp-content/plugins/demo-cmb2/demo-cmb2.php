<?php
/*
Plugin Name: Démo CMB2 WP Tech 2015
Description: Démo CMB2 pour le WP Tech 2015
Version: 1.0
Author: Patrick FAUST
Author URI: http://www.lipaonline.com/
License: GPLv2+
*/



/**
 * Get the bootstrap!
 */
if ( file_exists(  __DIR__ . '/cmb2/init.php' ) ) {
  require_once  __DIR__ . '/cmb2/init.php';
} elseif ( file_exists(  __DIR__ . '/CMB2/init.php' ) ) {
  require_once  __DIR__ . '/CMB2/init.php';
}
if ( file_exists(  __DIR__ . '/cmb-field-select2/cmb-field-select2.php' ) )
    require_once  __DIR__ . '/cmb-field-select2/cmb-field-select2.php';


if ( file_exists(  __DIR__ . '/cmb-field-umap/cmb-field-umap.php' ) )
    require_once  __DIR__ . '/cmb-field-umap/cmb-field-umap.php';

if ( file_exists(  __DIR__ . '/CMB2-Date-Range-Field/wds-cmb2-date-range-field.php' ) )
    require_once  __DIR__ . '/CMB2-Date-Range-Field/wds-cmb2-date-range-field.php';




add_action( 'cmb2_init', 'cmb2_demo_wptech' );
/**
 * Define the metabox and field configurations.
 */
function cmb2_demo_wptech() {



    /**
     * Initiate the metabox
     */


// Start with an underscore to hide fields from custom fields list
	$prefix = '_wptech_demo_';


    // dummy content

    $dummy_conf = array(

    'gates'         => 'Bill Gates',
    'mandela'       => 'Nelson Mandela',
    'morrison'      => 'Jim Morisson',
    'chewbacca'     => 'Chewbacca',
    'austin_power'  => 'Austin Powers '

    );


	/**
	 * Sample metabox to demonstrate each field type included
	 */
	$cmb = new_cmb2_box( array(
		'id'            => $prefix . 'metabox',
		'title'         => __( 'Informations Event', 'cmb2' ),
		'object_types'  => apply_filters( $prefix .'cpt_filter', array( 'page' )), // Post type
		// 'show_on_cb' => 'yourprefix_show_if_front_page', // function should return a bool value
		// 'context'    => 'normal',
		// 'priority'   => 'high',
		// 'show_names' => true, // Show field names on the left
		// 'cmb_styles' => false, // false to disable the CMB stylesheet
		// 'closed'     => true, // true to keep the metabox closed by default
	) );

    $cmb->add_field( array(
        'name'       => __( 'Date', 'cmb2' ),
        'desc'       => __( 'Début et fin de l\'évenement', 'cmb2' ),
        'id'         => $prefix . 'date_range',
        'type'       => 'date_range',
    ) );



    $cmb->add_field(array(
    'name'    => 'Conférenciers',
    'id'      => $prefix . 'conf',
    'type'    => 'pw_multiselect',
    'options' => apply_filters( $prefix .'conf_filter', $dummy_conf ) ,

    ));




    $cmb->add_field( array(
    'name' => 'Location',
    'id' => 'location',
    'type' => 'pw_map',
    'show_names'    => false,

) );



$group_field_id = $cmb->add_field( array(
    'id'          => 'sous_event_repeat_group',
    'type'        => 'group',
    'description' => __( 'Sous évenements', 'cmb' ),
    'options'     => array(
        'group_title'   => __( 'Évenement {#}', 'cmb' ), // since version 1.1.4, {#} gets replaced by row number
        'add_button'    => __( 'Ajouter un nouvel évenement', 'cmb' ),
        'remove_button' => __( 'Supprimer évenement', 'cmb' ),
        'sortable'      => true, // beta
        // 'closed'     => true, // true to have the groups closed by default
    ),
) );

// Id's for group's fields only need to be unique for the group. Prefix is not needed.
$cmb->add_group_field( $group_field_id, array(
    'name' => 'Titre',
    'id'   => 'title',
    'type' => 'text',
    // 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
) );

$cmb->add_group_field( $group_field_id, array(
    'name' => 'Description',
    'description' => 'Description',
    'id'   => 'description',
    'type' => 'textarea_small',
) );

$cmb->add_group_field( $group_field_id, array(
    'name' => 'Entry Image',
    'id'   => 'image',
    'type' => 'file',
) );


    $cmb->add_group_field( $group_field_id,array(
    'name' => 'Lieu',
    'id' => 'location',
    'type' => 'pw_map',
    'show_names'    => false,

) );




}
